package com.skillup;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MyTest extends TestRunner {
    private static final String EXPECTED_URL = "https://www.youtube.com/";
    private static final String URL = "https://www.youtube.com";
    private static final String UNEXPECTED_URL = "https://www.youtube.co";
    private static final String EXPECTED_URL_TEXT = "youtube";
    private static final String UNEXPECTED_URL_TEXT = "Strange Text";


    @Test
    public void myTest() {
        driver.get(URL);
        String actualUrl = driver.getCurrentUrl();


//        Assert.assertTrue(EXPECTED_URL.equals(actualUrl));
//        Assert.assertTrue(false);

        Assert.assertEquals(actualUrl, EXPECTED_URL, "SITE TITLE IS WRONG");

        driver.quit();


    }

    @Test
    public void myTest2() {
        driver.get(URL);
        String actualUrl = driver.getCurrentUrl();


//        Assert.assertTrue(EXPECTED_URL.equals(actualUrl));
//        Assert.assertTrue(false);

        Assert.assertEquals(actualUrl, EXPECTED_URL, "SITE TITLE IS WRONG");

        driver.quit();


    }

    @Test
    public void myTest3() {
        driver.get(URL);
        String actualUrl = driver.getCurrentUrl();


//        Assert.assertTrue(EXPECTED_URL.equals(actualUrl));
//        Assert.assertTrue(false);

        Assert.assertEquals(actualUrl, EXPECTED_URL, "SITE TITLE IS WRONG");

        driver.quit();


    }

}
